﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using InventoryServer.Extensions;
using InventoryServer.Model;

namespace InventoryServer
{
    public class InventoryHandler : Script
    {
        #region Properties & Fields

        private readonly List<Player> _players;
        private readonly List<CollectSpot> _collectSpots;
        private readonly List<House> _houses;
        private readonly List<VehicleServer> _vehicles;

        #endregion

        #region Constructors

        public InventoryHandler()
        {
            _players = new List<Player>();
            _collectSpots = new List<CollectSpot>();
            _houses = new List<House>();
            _vehicles = new List<VehicleServer>();
        }

        #endregion

        #region Events

        [ServerEvent(Event.PlayerConnected)]
        public void PlayerConnect(Client client)
        {
            _players.Add(new Player
            {
                Client = client,
                Id = _players.Count + 1,
                TempId = _players.Count + 1,
                Username = client.Name
            });
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void PlayerDisconnect(Client client, DisconnectionType type, string reason)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            _players.Remove(player);
        }

        #endregion

        #region Commands

        [Command("createvehicle", Alias = "cv")]
        public void CreateVehicleCommand(Client client, string model, string numberplate)
        {
            VehicleServer vehicleServer = new VehicleServer
            {
                Numberplate = numberplate
            };

            NAPI.Task.Run(() =>
            {
                vehicleServer.Handle = NAPI.Vehicle.CreateVehicle(NAPI.Util.VehicleNameToModel(model), client.Position, client.Rotation, 0, 0, numberplate);
            });

            _vehicles.Add(vehicleServer);
        }

        [Command("createhouse", Alias = "ch")]
        public void CreateHouseCommand(Client client)
        {
            House house = new House
            {
                Id = _houses.Count + 1
            };

            NAPI.Task.Run(() =>
            {
                ColShape colShape = NAPI.ColShape.CreateCylinderColShape(client.Position, 2f, 5f, client.Dimension);
                colShape.SetData("ColshapeId", house.Id);
                colShape.OnEntityEnterColShape += (shape, entity) =>
                {
                    if (entity.Type != EntityType.Player || NAPI.Player.IsPlayerInAnyVehicle(entity))
                        return;

                    Player player = _players.FirstOrDefault(x => x.Username == entity.Name);

                    if (player == null)
                        return;

                    player.HouseId = shape.GetData("ColshapeId");
                };
                colShape.OnEntityExitColShape += (shape, entity) =>
                {
                    if (entity.Type != EntityType.Player)
                        return;

                    Player player = _players.FirstOrDefault(x => x.Username == entity.Name);

                    if (player == null)
                        return;

                    player.HouseId = -1;
                };

                NAPI.Marker.CreateMarker(MarkerType.VerticalCylinder, client.Position, new Vector3(), new Vector3(), 2f, new Color(255, 0, 0));
            });

            _houses.Add(house);
        }

        [Command("createcollectspot", Alias = "ccs")]
        public void CreateCollectSpotCommand(Client client)
        {
            CollectSpot collectSpot = new CollectSpot
            {
                Id = _collectSpots.Count + 1
            };

            NAPI.Task.Run(() =>
            {
                ColShape colShape = NAPI.ColShape.CreateCylinderColShape(client.Position, 4f, 4f, client.Dimension);
                colShape.SetData("ColshapeId", collectSpot.Id);
                colShape.OnEntityEnterColShape += (shape, entity) =>
                {
                    if (entity.Type != EntityType.Player || NAPI.Player.IsPlayerInAnyVehicle(entity))
                        return;

                    Player player = _players.FirstOrDefault(x => x.Username == entity.Name);

                    if (player == null)
                        return;

                    player.CollectSpotId = shape.GetData("ColshapeId");
                };
                colShape.OnEntityExitColShape += (shape, entity) =>
                {
                    if (entity.Type != EntityType.Player)
                        return;

                    Player player = _players.FirstOrDefault(x => x.Username == entity.Name);

                    if (player == null)
                        return;

                    player.CollectSpotId = -1;
                };

                NAPI.Marker.CreateMarker(MarkerType.VerticalCylinder, client.Position, new Vector3(), new Vector3(), 4f, new Color(0, 255, 0));
            });

            _collectSpots.Add(collectSpot);
        }

        [Command("addbottle", Alias = "ab")]
        public void AddBottleCommand(Client client)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            player?.AddInventoryItem(new InventoryItem(InventoryItems.Bottle));
        }

        [Command("addmissing", Alias = "am")]
        public void AddMissingCommand(Client client)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            player?.AddInventoryItem(new InventoryItem(InventoryItems.Missing));
        }

        #endregion

        #region RemoteEvents

        [RemoteEvent("tradeInventoryItem")]
        public void TradeInventoryItem(Client client, object[] arguments)
        {
            string inventoryItem = arguments[0].ToString();
            double amount = double.Parse(arguments[1].ToString());
            int sourceInventoryType = int.Parse(arguments[2].ToString());
            int targetInventoryType = int.Parse(arguments[3].ToString());
            string tradingId = arguments[4].ToString();

            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            InventoryItems? inventoryItemType = null;

            try
            {
                inventoryItemType = EnumExtensions.GetValueFromDescription<InventoryItems>(inventoryItem);
            }
            catch (ArgumentException)
            { }

            if (!inventoryItemType.HasValue)
                return;

            Inventory sourceInventory = null;

            switch (sourceInventoryType)
            {
                case 1: //current player
                    sourceInventory = player;
                    break;
                case 2: //other player
                    sourceInventory = null;
                    break;
                case 3: //vehicle
                    sourceInventory = _vehicles.FirstOrDefault(x => x.Numberplate == tradingId);
                    break;
                case 4: //house
                    sourceInventory = _houses.FirstOrDefault(x => x.Id == int.Parse(tradingId));
                    break;
                case 5: //collectSpot
                    sourceInventory = _collectSpots.FirstOrDefault(x => x.Id == int.Parse(tradingId));
                    break;
            }

            if (sourceInventory == null)
                return;

            Inventory targetInventory = null;

            switch (targetInventoryType)
            {
                case 1: //current player
                    targetInventory = player;
                    break;
                case 2: //other player
                    targetInventory = _players.FirstOrDefault(x => x.TempId == int.Parse(tradingId));
                    break;
                case 3: //vehicle
                    targetInventory = _vehicles.FirstOrDefault(x => x.Numberplate == tradingId);
                    break;
                case 4: //house
                    targetInventory = _houses.FirstOrDefault(x => x.Id == int.Parse(tradingId));
                    break;
                case 5: //collectSpot
                    targetInventory = _collectSpots.FirstOrDefault(x => x.Id == int.Parse(tradingId));
                    break;
            }

            if (targetInventory == null)
                return;

            if (!sourceInventory.HasInventoryItem(inventoryItemType.Value))
                return;

            if (amount <= 0)
                return;

            if (amount > sourceInventory.GetInventoryItemQuantity(inventoryItemType.Value))
                amount = sourceInventory.GetInventoryItemQuantity(inventoryItemType.Value);

            InventoryItem item = new InventoryItem(inventoryItemType.Value, amount);

            sourceInventory.RemoveInventoryItem(item);

            if (!targetInventory.AddInventoryItem(item))
            {
                sourceInventory.AddInventoryItem(item);
                client.SendNotification("Your inventory is full.");
                return;
            }

            if (targetInventoryType == 2)
                ((Player)targetInventory).Client.SendNotification(string.Format("You get {0} {1}", item.Quantity, item.Type.GetDescription()));
        }

        [RemoteEvent("getInventory")]
        public void GetInventory(Client client, object[] arguments)
        {
            int inventoryType = int.Parse(arguments[0].ToString());
            string tradingId = arguments[1].ToString();

            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            Inventory inventory = null;

            switch (inventoryType)
            {
                case 1: //current player
                    inventory = player;
                    break;
                case 2: //other player
                    inventory = null;
                    break;
                case 3: //vehicle
                    inventory = _vehicles.FirstOrDefault(x => x.Numberplate == tradingId);
                    break;
                case 4: //house
                    inventory = _houses.FirstOrDefault(x => x.Id == int.Parse(tradingId));
                    break;
                case 5: //collectSpot
                    inventory = _collectSpots.FirstOrDefault(x => x.Id == int.Parse(tradingId));
                    break;
            }

            string inventoryItems = string.Empty;
            int id = 1;

            if (inventory != null)
            {
                foreach (InventoryItem inventoryItem in inventory.GetInventoryItems())
                {
                    if (!string.IsNullOrEmpty(inventoryItems))
                        inventoryItems += ";";

                    //ItemDescription:ItemQuantity:InventoryType:ItemId:ItemUsable:ItemQuantityDecimal
                    inventoryItems += $"{inventoryItem.Type.GetDescription()}:{inventoryItem.Quantity}:{inventoryType}{id}:0:0";
                    id++;
                }
            }

            client.TriggerEvent("fillInventory", inventoryItems, inventoryType, tradingId);
        }

        [RemoteEvent("showInventory")]
        public void ShowInventory(Client client, object[] arguments)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            string inventoryItems = string.Empty;
            int id = 1;

            foreach (InventoryItem inventoryItem in player.GetInventoryItems())
            {
                if (!string.IsNullOrEmpty(inventoryItems))
                    inventoryItems += ";";

                //ItemDescription:ItemQuantity:InventoryType:ItemId:ItemUsable:ItemQuantityDecimal
                inventoryItems += $"{inventoryItem.Type.GetDescription()}:{inventoryItem.Quantity}:1{id}:0:0";
                id++;
            }

            string tradingPartners = string.Empty;

            foreach (Player playerInRadius in FindPlayersInRadius(player.Client.Position, 1f))
            {
                if (player.Id == playerInRadius.Id)
                    continue;

                if (!string.IsNullOrEmpty(tradingPartners))
                    tradingPartners += ";";

                tradingPartners += $"{playerInRadius.TempId}:2";
            }

            foreach (VehicleServer vehicleServer in FindVehiclesInRadius(player.Client.Position, 5f))
            {
                if (!string.IsNullOrEmpty(tradingPartners))
                    tradingPartners += ";";

                tradingPartners += $"{vehicleServer.Numberplate}:3";
            }

            if (player.HouseId != -1)
            {
                if (!string.IsNullOrEmpty(tradingPartners))
                    tradingPartners += ";";

                tradingPartners += $"{player.HouseId}:4";
            }

            if (player.CollectSpotId != -1)
            {
                if (!string.IsNullOrEmpty(tradingPartners))
                    tradingPartners += ";";

                tradingPartners += $"{player.CollectSpotId}:5";
            }

            client.TriggerEvent("showInventory", inventoryItems, tradingPartners);
        }

        [RemoteEvent("useInventoryItem")]
        public void UseInventoryItem(Client client, object[] arguments)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            string inventoryItem = arguments[0].ToString();
            double amount = double.Parse(arguments[1].ToString());

            InventoryItems? inventoryItemType = null;

            try
            {
                inventoryItemType = EnumExtensions.GetValueFromDescription<InventoryItems>(inventoryItem);
            }
            catch (ArgumentException)
            { }

            if (!inventoryItemType.HasValue)
                return;

            if (amount <= 0)
                return;

            if (!player.HasInventoryItem(inventoryItemType.Value))
                return;

            if (amount < 0)
                return;

            if (amount > player.GetInventoryItemQuantity(inventoryItemType.Value))
                amount = player.GetInventoryItemQuantity(inventoryItemType.Value);

            InventoryItem item = new InventoryItem(inventoryItemType.Value, amount);

            player.RemoveInventoryItem(item);
        }

        [RemoteEvent("removeInventoryItem")]
        public void RemoveInventoryItem(Client client, object[] arguments)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            string inventoryItem = arguments[0].ToString();
            double amount = double.Parse(arguments[1].ToString());

            InventoryItems? inventoryItemType = null;

            try
            {
                inventoryItemType = EnumExtensions.GetValueFromDescription<InventoryItems>(inventoryItem);
            }
            catch (ArgumentException)
            { }

            if (!inventoryItemType.HasValue)
                return;

            if (amount <= 0)
                return;

            if (!player.HasInventoryItem(inventoryItemType.Value))
                return;

            if (amount > player.GetInventoryItemQuantity(inventoryItemType.Value))
                amount = player.GetInventoryItemQuantity(inventoryItemType.Value);

            InventoryItem item = new InventoryItem(inventoryItemType.Value, amount);

            player.RemoveInventoryItem(item);
        }

        #endregion

        #region HelperMethods

        private List<Player> FindPlayersInRadius(Vector3 pos, float range)
        {
            List<Player> playerlist = new List<Player>();

            foreach (Player p in _players)
                if (p != null)
                {
                    if (p.Client != null)
                        if (Vector3.Distance(pos, p.Client.Position) < range)
                            playerlist.Add(p);
                }

            return playerlist;
        }

        private List<VehicleServer> FindVehiclesInRadius(Vector3 pos, float range)
        {
            List<VehicleServer> vehiclelist = new List<VehicleServer>();

            foreach (VehicleServer v in _vehicles)
                if (v?.Handle != null && Vector3.Distance(pos, v.Handle.Position) < range)
                    vehiclelist.Add(v);

            return vehiclelist;
        }

        #endregion
    }
}
