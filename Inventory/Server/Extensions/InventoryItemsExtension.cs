﻿using System.ComponentModel;
using System.Reflection;
using InventoryServer.Attributes;

namespace InventoryServer.Extensions
{
    public static class InventoryItemsExtension
    {
        public static int GetWeight(this object enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(WeightAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((WeightAttribute)attrs[0]).Value;
            }

            return 0;
        }

        public static string GetDescription(this object enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return string.Empty;
        }
    }
}
