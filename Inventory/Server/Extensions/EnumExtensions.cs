﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace InventoryServer.Extensions
{
    public static class EnumExtensions
    {
        public static T GetValueFromDescription<T>(string description, bool ignoreCase = false)
        {
            Type type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (FieldInfo field in type.GetFields())
            {
                DescriptionAttribute attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (ignoreCase && attribute.Description.ToLower() == description.ToLower())
                        return (T)field.GetValue(null);

                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (ignoreCase && field.Name.ToLower() == description.ToLower())
                        return (T)field.GetValue(null);

                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentException("Not found.", "description");
        }
    }
}
