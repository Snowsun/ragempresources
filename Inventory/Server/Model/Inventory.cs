﻿using System.Collections.Generic;
using System.Linq;
using InventoryServer.Extensions;

namespace InventoryServer.Model
{
    public class Inventory
    {
        private readonly List<InventoryItem> _inventoryItems;

        public Inventory()
        {
            _inventoryItems = new List<InventoryItem>();
        }

        public virtual bool AddInventoryItem(InventoryItem itemToAdd)
        {
            if (GetInventoryUsedSize() + itemToAdd.Quantity * itemToAdd.Type.GetWeight() > GetInventorySize())
                return false;

            if (_inventoryItems.Any(x => x.Type == itemToAdd.Type))
            {
                _inventoryItems.First(x => x.Type == itemToAdd.Type).Quantity += itemToAdd.Quantity;
                _inventoryItems.First(x => x.Type == itemToAdd.Type).Changed = true;
            }
            else
            {
                itemToAdd.Changed = true;
                _inventoryItems.Add(itemToAdd);
            }

            return true;
        }

        public virtual bool AddInventoryItemAtStart(InventoryItem itemToAdd)
        {
            if (GetInventoryUsedSize() + itemToAdd.Quantity * itemToAdd.Type.GetWeight() > GetInventorySize())
                return false;

            if (_inventoryItems.Any(x => x.Type == itemToAdd.Type))
                _inventoryItems.First(x => x.Type == itemToAdd.Type).Quantity += itemToAdd.Quantity;
            else
                _inventoryItems.Add(itemToAdd);

            return true;
        }

        public virtual void RemoveInventoryItem(InventoryItem itemToRemove)
        {
            if (_inventoryItems.All(x => x.Type != itemToRemove.Type))
                return;

            InventoryItem item = _inventoryItems.First(x => x.Type == itemToRemove.Type);

            item.Quantity -= itemToRemove.Quantity;
            item.Changed = true;
        }

        public virtual List<InventoryItem> GetInventoryItems()
        {
            return _inventoryItems.Where(x => x.Quantity > 0).ToList();
        }

        public virtual List<InventoryItem> GetAllInventoryItems()
        {
            return _inventoryItems;
        }

        public virtual bool HasInventoryItem(InventoryItems type)
        {
            return _inventoryItems.Any(x => x.Type == type && x.Quantity > 0);
        }

        public virtual double GetInventoryItemQuantity(InventoryItems type)
        {
            return _inventoryItems.FirstOrDefault(x => x.Type == type)?.Quantity ?? 0.0;
        }

        public virtual void CleanInventory()
        {
            foreach (InventoryItem item in _inventoryItems)
            {
                item.Quantity = 0;
                item.Changed = true;
            }
        }

        public double GetInventoryUsedSize()
        {
            double usedSize = 0;

            foreach (InventoryItem inventoryItem in _inventoryItems)
                usedSize += inventoryItem.Quantity * inventoryItem.Type.GetWeight();

            return usedSize;
        }

        public virtual int GetInventorySize()
        {
            return 0;
        }
    }
}
