﻿namespace InventoryServer.Model
{
    public class InventoryItem
    {
        public InventoryItems Type { get; private set; }
        public double Quantity { get; set; }
        public bool Changed { get; set; }

        public InventoryItem(InventoryItems type, double quantity = 1)
        {
            Type = type;
            Quantity = quantity;
        }
    }
}
