﻿using System;

namespace InventoryServer.Attributes
{
    public class WeightAttribute : Attribute
    {
        public int Value { get; private set; }

        public WeightAttribute(int value)
        {
            Value = value;
        }
    }
}
