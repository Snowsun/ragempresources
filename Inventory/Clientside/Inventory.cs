﻿using RAGE;

namespace InventoryClientside
{
    public class Inventory : Events.Script
    {
        private CefObject _cefInventory = null;

        public Inventory()
        {
            Events.Add("getInventory", GetInventoryCall);
            Events.Add("getPlayerKeys", GetPlayerKeysCall);
            Events.Add("tradeInventoryItem", TradeInventoryItemCall);
            Events.Add("tradeKey", TradeKeyCall);
            Events.Add("removeInventoryItem", RemoveInventoryItemCall);
            Events.Add("useInventoryItem", UseInventoryItemCall);
            Events.Add("givePlayerMoney", GivePlayerMoneyCall);
            Events.Add("showPlayerPerso", ShowPlayerPersoCall);
            Events.Add("iKeyPressed", IKeyPressed);
            Events.Add("showInventory", ShowInventory);
            Events.Add("fillInventory", FillInventory);
            Events.Add("fillPlayerKeys", FillPlayerKeys);
        }

        private void GetInventoryCall(object[] args)
        {
            Events.CallRemote("getInventory", args[0].ToString(), args[1].ToString());
        }

        private void GetPlayerKeysCall(object[] args)
        {
            Events.CallRemote("getPlayerKeys");
        }

        private void TradeInventoryItemCall(object[] args)
        {
            Events.CallRemote("tradeInventoryItem", args[0].ToString(), args[1].ToString(), args[2].ToString(), args[3].ToString(), args[4].ToString());
        }

        private void TradeKeyCall(object[] args)
        {
            Events.CallRemote("tradeKey", args[0].ToString(), args[1].ToString(), args[2].ToString());
        }

        private void RemoveInventoryItemCall(object[] args)
        {
            Events.CallRemote("removeInventoryItem", args[0].ToString(), args[1].ToString());
        }

        private void UseInventoryItemCall(object[] args)
        {
            Events.CallRemote("useInventoryItem", args[0].ToString(), args[1].ToString());
        }

        private void GivePlayerMoneyCall(object[] args)
        {
            Events.CallRemote("givePlayerMoney", args[0].ToString(), args[1].ToString());
        }

        private void ShowPlayerPersoCall(object[] args)
        {
            Events.CallRemote("showPersoTo", args[0].ToString());
        }

        private void IKeyPressed(object[] args)
        {
            if (_cefInventory != null)
            {
                CefHelper.DestroyCefBrowser(_cefInventory);
                _cefInventory = null;
            }
            else
            {
                Events.CallRemote("showInventory");
            }
        }

        private void ShowInventory(object[] args)
        {
            if (_cefInventory != null)
            {
                CefHelper.DestroyCefBrowser(_cefInventory);
                _cefInventory = null;
            }

            string inventoryItems = args[0].ToString();
            string tradingPartners = args[1].ToString();

            _cefInventory = CefHelper.CreateCefBrowser("package://cs_packages/Inventory/resources/inventory.html");
            _cefInventory.Execute($"fillOwnInventory(\"{inventoryItems}\")");
            _cefInventory.Execute($"fillTradingPartners(\"{tradingPartners}\")");
        }

        private void FillInventory(object[] args)
        {
            if (_cefInventory == null)
                return;

            string inventoryItems = args[0].ToString();
            int inventoryType = int.Parse(args[1].ToString());
            string tradingId = args[2].ToString();

            if (inventoryType == 1)
                _cefInventory.Execute($"fillOwnInventory(\"{inventoryItems}\")");
            else
                _cefInventory.Execute($"fillTradingInventory(\"{inventoryItems}\", {inventoryType}, \"{tradingId}\")");
        }

        private void FillPlayerKeys(object[] args)
        {
            if (_cefInventory == null)
                return;

            string playerKeys = args[0].ToString();

            _cefInventory.Execute($"fillOwnInventory(\"{playerKeys}\")");
        }
    }
}
