﻿using System;
using System.Threading.Tasks;
using GTANetworkAPI;

namespace HudServer
{
    public class HudHandler : Script
    {
        private DateTime? _lastTimeMinuteUpdate;

        [ServerEvent(Event.PlayerConnected)]
        public void PlayerConnect(Client client)
        {
            client.TriggerEvent("showHud", DateTime.Now.ToString("dd.MM.yyyy | HH:mm"), Convert.ToString(0));
        }

        [ServerEvent(Event.Update)]
        public void OnUpdate()
        {
            if (_lastTimeMinuteUpdate == null ||
                DateTime.Now.Subtract(_lastTimeMinuteUpdate.Value).TotalMinutes >= 1) // Every minute
            {
                DateTime src = DateTime.Now;
                _lastTimeMinuteUpdate = new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minute, 0);

                Task.Run(() =>
                {
                    UpdateClientTimePlayers();
                });
            }
        }

        private void UpdateClientTimePlayers()
        {
            if (!_lastTimeMinuteUpdate.HasValue)
                return;

            foreach (Client client in NAPI.Pools.GetAllPlayers())
                client.TriggerEvent("dateAndTimeChanged", _lastTimeMinuteUpdate.Value.ToString("dd.MM.yyyy | HH:mm"));
        }
    }
}
