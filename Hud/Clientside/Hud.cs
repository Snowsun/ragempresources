﻿using System;
using System.Collections.Generic;
using RAGE;
using RAGE.Elements;

namespace HudClientside
{
    public class Hud : Events.Script
    {
        private CefObject _cefHud = null;

        private bool _isEngineOn = false;
        private bool _isVehicleLocked = false;
        private float _engineHealth = 0f;
        private int _actualKmStand = -1;
        private int _actualSpeed = -1;
        private bool _isVehicleHudVisible = false;

        public Hud()
        {
            Events.Tick += OnUpdate;
            Events.Add("vehicleLockedChanged", VehicleLockedChanged);
            Events.Add("vehicleEngineStatusChanged", VehicleEngineStatusChanged);
            Events.Add("vehicleHealthUpdated", VehicleHealthUpdated);
            Events.Add("dateAndTimeChanged", DateAndTimeChanged);
            Events.Add("moneyChanged", MoneyChanged);
            Events.Add("showHud", ShowHud);
            Events.Add("distanceChannelChanged", DistanceChannelChanged);
        }

        private void DistanceChannelChanged(object[] args)
        {
            int distanceChannel = (int)args[0];

            _cefHud?.Execute($"setDistanceChannel({distanceChannel})");
        }

        private void ShowHud(object[] args)
        {
            if (_cefHud != null)
            {
                CefHelper.DestroyCefBrowser(_cefHud);
                _cefHud = null;
            }

            _cefHud = CefHelper.CreateCefBrowser("package://cs_packages/Hud/resources/hud.html", isHud: true, destroyAction: HudDestroied);
            _cefHud.Execute($"setDateAndTime(\"{args[0].ToString()}\")");
            _cefHud.Execute($"setMoney(\"{args[1].ToString()}\")");
            _cefHud.Execute("setDistanceChannel(1)");
        }

        private void HudDestroied()
        {
            _cefHud = null;
        }

        private void OnUpdate(List<Events.TickNametagData> nametags)
        {
            if (_cefHud == null)
                return;

            if (!Player.LocalPlayer.IsInAnyVehicle(false))
            {
                if (_isVehicleHudVisible)
                {
                    _cefHud?.Execute("hideVehicleStats()");
                    _isVehicleHudVisible = false;
                }

                return;
            }

            Vehicle veh = Player.LocalPlayer.Vehicle;

            if (veh.GetClass() == 13)
                return;

            if (!_isVehicleHudVisible)
            {
                _isVehicleLocked = veh.GetDoorLockStatus() == 0;
                _isEngineOn = veh.GetIsEngineRunning();
                _engineHealth = veh.GetEngineHealth();

                _cefHud.Execute($"setVehicleLockedState(\"{_isVehicleLocked}\")");
                _cefHud.Execute($"setVehicleEngineState(\"{_isEngineOn}\")");
                _cefHud.Execute($"setVehicleEngineHealth(\"{_engineHealth}\")");
                _cefHud.Execute("showVehicleStats()");
                _isVehicleHudVisible = true;
            }

            Vector3 velocity = veh.GetVelocity();
            int speed = (int)(Math.Sqrt(
                                     velocity.X * velocity.X +
                                     velocity.Y * velocity.Y +
                                     velocity.Z * velocity.Z
                                 ) * 3.6);

            float kmstand = 0;

            if (veh.GetSharedData("KmStand") != null)
                kmstand = (float)veh.GetSharedData("KmStand");


            int kmstandanzeige = (int)Math.Round(kmstand * 100) / 100;

            if (_actualKmStand != kmstandanzeige)
            {
                _actualKmStand = kmstandanzeige;
                _cefHud.Execute($"setVehicleKm(\"{_actualKmStand.ToString()}\")");
            }

            if (_actualSpeed != speed)
            {
                _actualSpeed = speed;
                _cefHud.Execute($"setVehicleKmh(\"{_actualSpeed.ToString()}\")");
            }
        }

        private void VehicleLockedChanged(object[] args)
        {
            _isVehicleLocked = !_isVehicleLocked;

            _cefHud?.Execute($"setVehicleLockedState(\"{_isVehicleLocked}\")");
        }

        private void VehicleEngineStatusChanged(object[] args)
        {
            _isEngineOn = !_isEngineOn;

            _cefHud?.Execute($"setVehicleEngineState(\"{_isEngineOn}\")");
        }

        private void VehicleHealthUpdated(object[] args)
        {
            if (!Player.LocalPlayer.IsInAnyVehicle(false))
                return;

            _engineHealth = float.Parse(args[1].ToString());

            _cefHud?.Execute($"setVehicleEngineHealth(\"{_engineHealth}\")");
        }

        private void DateAndTimeChanged(object[] args)
        {
            _cefHud?.Execute($"setDateAndTime(\"{args[0].ToString()}\")");
        }

        private void MoneyChanged(object[] args)
        {
            _cefHud?.Execute($"setMoney(\"{args[0].ToString()}\")");
        }
    }
}
