﻿using System;
using System.Collections.Generic;
using System.Linq;
using RAGE;

namespace VoiceClientside
{
    public class KeyHandler : Events.Script
    {
        private readonly Dictionary<ConsoleKey, bool> _keyList = new Dictionary<ConsoleKey, bool>();
        private bool _isChatActive = false;

        private delegate void EventKeyDelegate(ConsoleKey key);
        private EventKeyDelegate KeyDownEvent { get; set; }
        private EventKeyDelegate KeyUpEvent { get; set; }

        public KeyHandler()
        {
            _keyList.Add(ConsoleKey.X, false);
            _keyList.Add(ConsoleKey.F9, false);
            _keyList.Add(ConsoleKey.LeftArrow, false);
            _keyList.Add(ConsoleKey.RightArrow, false);
            _keyList.Add(ConsoleKey.T, false);
            _keyList.Add(ConsoleKey.Enter, false);

            Events.Tick += OnUpdate;

            KeyDownEvent += OnKeyPressed;
            KeyUpEvent += OnKeyReleased;
        }

        private void OnKeyPressed(ConsoleKey key)
        {
            if (key == ConsoleKey.T && !_isChatActive)
                _isChatActive = true;

            if (_isChatActive && key == ConsoleKey.Enter)
                _isChatActive = false;

            if (_isChatActive)
                return;

            if (key == ConsoleKey.F9)
                Events.CallLocal("f9KeyPressed");

            if (key == ConsoleKey.X)
                Events.CallLocal("xKeyPressed");

            if (key == ConsoleKey.LeftArrow)
                Events.CallLocal("leftArrowPressed");

            if (key == ConsoleKey.RightArrow)
                Events.CallLocal("rightArrowPressed");
        }

        private void OnKeyReleased(ConsoleKey key)
        {
            if (_isChatActive)
                return;

            if (key == ConsoleKey.X)
                Events.CallLocal("xKeyReleased");
        }

        private void OnUpdate(List<Events.TickNametagData> nametags)
        {
            foreach (KeyValuePair<ConsoleKey, bool> key in _keyList.ToList())
            {
                if (_keyList[key.Key] && !Input.IsDown((int)key.Key))
                {
                    KeyUpEvent?.Invoke(key.Key);
                    _keyList[key.Key] = false;
                }
                else if (!_keyList[key.Key] && Input.IsDown((int)key.Key))
                {
                    KeyDownEvent?.Invoke(key.Key);
                    _keyList[key.Key] = true;
                }
            }
        }
    }
}
