﻿using System;
using System.Collections.Generic;
using System.Linq;
using RAGE;
using RAGE.Elements;
using RAGE.Game;
using Entity = RAGE.Elements.Entity;
using Player = RAGE.Elements.Player;

namespace VoiceClientside
{
    public class Voice : Events.Script
    {
        private long _latestProcess = 0;
        private int _distanceChannel = 1;
        private readonly HashSet<Player> _activePlayers = new HashSet<Player>();
        private readonly HashSet<Player> _radioPlayers = new HashSet<Player>();
        private readonly HashSet<Player> _streamedPlayers = new HashSet<Player>();
        private Player _phonePlayer = null;

        public Voice()
        {
            Events.Add("playerConnected", PlayerConnected);
            Events.Add("f9KeyPressed", MuteKeyPressed);
            Events.Add("xKeyPressed", EnableRadio);
            Events.Add("xKeyReleased", DisableRadio);
            Events.Add("addVoiceRadioPlayer", AddVoiceRadioPlayer);
            Events.Add("removeVoiceRadioPlayer", RemoveVoiceRadioPlayer);
            Events.Add("clearVoiceRadioPlayers", ClearVoiceRadioPlayers);
            Events.Add("changeVoicePhonePlayer", ChangeVoicePhonePlayer);
            Events.Add("voiceRangeTest", VoiceRangeTest);
            Events.Add("leftArrowPressed", LeftArrowPressed);
            Events.Add("rightArrowPressed", RightArrowPressed);
            Events.Tick += VoiceProcess;
            Events.OnPlayerQuit += RemoveFromVoice;
            Events.OnEntityStreamIn += OnEntityStreamIn;
            Events.OnEntityStreamOut += OnEntityStreamOut;
        }

        private void RightArrowPressed(object[] args)
        {
            if (RAGE.Voice.Muted)
                return;

            _distanceChannel++;

            if (_distanceChannel > 3)
                _distanceChannel = 3;

            SetDistanceChannel();
            SetMaxRange();
        }

        private void LeftArrowPressed(object[] args)
        {
            if (RAGE.Voice.Muted)
                return;

            _distanceChannel--;

            if (_distanceChannel < 1)
                _distanceChannel = 1;

            SetDistanceChannel();
            SetMaxRange();
        }

        private void SetMaxRange()
        {
            switch (_distanceChannel)
            {
                case 1:
                    Events.CallRemote("SetMaxDistance", 25f);
                    break;
                case 2:
                    Events.CallRemote("SetMaxDistance", 60f);
                    break;
                case 3:
                    Events.CallRemote("SetMaxDistance", 100f);
                    break;
            }
        }

        private void OnEntityStreamIn(Entity entity)
        {
            if (!(entity is Player p) || p.Id == Player.LocalPlayer.Id)
                return;

            _streamedPlayers.Add((Player)entity);
        }

        private void OnEntityStreamOut(Entity entity)
        {
            if (!(entity is Player p) || p.Id == Player.LocalPlayer.Id)
                return;

            if (_streamedPlayers.Contains(p))
                _streamedPlayers.Remove(p);
        }

        private void VoiceRangeTest(object[] args)
        {
            Vector3 localPosition = Player.LocalPlayer.Position;

            Chat.Output("Streamed Players");

            foreach (Player player in _streamedPlayers)
            {
                float dist = (localPosition.DistanceToSquared(player.Position));
                bool listening = player.GetData<bool>("IsListening");

                Chat.Output($"{player.Name} {dist.ToString("N")} {(listening ? "ja" : "nein")}");
            }

            Chat.Output("Active Players");

            foreach (Player player in _activePlayers)
            {
                if (player.Handle != 0)
                {
                    float dist = (localPosition.DistanceToSquared(player.Position));
                    float maxDistance = (float)player.GetSharedData("MaxDistance");

                    float volumn = (1.0f - (dist / maxDistance));
                    Chat.Output($"{player.Name} {dist.ToString("N")} {volumn.ToString("N")} {player.VoiceVolume.ToString("N")}");
                }
            }

            Chat.Output("Radio Players");

            foreach (Player player in _radioPlayers)
            {
                if (player.Handle != 0)
                    Chat.Output($"{player.Name}");
            }

            Chat.Output("Phone Player");

            if (_phonePlayer != null)
                Chat.Output($"{_phonePlayer.Name}");

            Chat.Output("Distance Channel");

            Chat.Output($"{_distanceChannel}");
            Chat.Output($"{(float)Player.LocalPlayer.GetSharedData("MaxDistance")}");
        }

        private void DisableRadio(object[] args)
        {
            Events.CallRemote("SetIsRadioTalking", false);
        }

        private void EnableRadio(object[] args)
        {
            Events.CallRemote("SetIsRadioTalking", true);
        }

        private void ChangeVoicePhonePlayer(object[] args)
        {
            Player newPlayer = null;

            if (!string.IsNullOrEmpty(args[0].ToString()))
                newPlayer = Entities.Players.All.FirstOrDefault(x => x.Name == args[0].ToString());

            if (_phonePlayer != null &&
               !_activePlayers.Contains(_phonePlayer) &&
               !_radioPlayers.Contains(_phonePlayer))
                _phonePlayer.VoiceVolume = 0f;

            _phonePlayer = newPlayer;
        }

        private void ClearVoiceRadioPlayers(object[] args)
        {
            _radioPlayers.Clear();
        }

        private void RemoveVoiceRadioPlayer(object[] args)
        {
            Player target = Entities.Players.All.FirstOrDefault(x => x.Name == args[0].ToString());

            if (target != null)
                _radioPlayers.Remove(target);
        }

        private void AddVoiceRadioPlayer(object[] args)
        {
            Player target = Entities.Players.All.FirstOrDefault(x => x.Name == args[0].ToString());

            if (target != null)
                _radioPlayers.Add(target);
        }

        private void MuteKeyPressed(object[] args)
        {
            RAGE.Voice.Muted = !RAGE.Voice.Muted;
            SetDistanceChannel();
        }

        private void SetDistanceChannel()
        {
            if (RAGE.Voice.Muted)
                Events.CallLocal("distanceChannelChanged", 0);
            else
                Events.CallLocal("distanceChannelChanged", _distanceChannel);
        }

        private void PlayerConnected(object[] args)
        {
            _activePlayers.Clear();
            _radioPlayers.Clear();
            _phonePlayer = null;
            RAGE.Voice.Muted = false;
        }

        private void Add(Player player)
        {
            player.SetData("IsListening", true);
            Events.CallRemote("add_voice_listener", player.Name);

            _activePlayers.Add(player);
        }

        private void Remove(Player player, bool notify)
        {
            _activePlayers.Remove(player);
            player.VoiceVolume = 0f;
            player.Voice3d = false;

            player.SetData("IsListening", false);

            if (notify)
                Events.CallRemote("remove_voice_listener", player.Name);
        }

        private void RemoveFromVoice(Player player)
        {
            Remove(player, false);
        }

        private void VoiceProcess(List<Events.TickNametagData> nametags)
        {
            long currentMilliseconds = UnixTimeNowInMilliseconds();

            if ((currentMilliseconds - _latestProcess) <= 250)
                return;

            _latestProcess = currentMilliseconds;

            Vector3 localPosition = Player.LocalPlayer.Position;

            foreach (Player playerTempStreamed in _streamedPlayers.ToList())
            {
                Player player = Entities.Players.GetAt(playerTempStreamed.Id);

                if (!player.GetData<bool>("IsListening"))
                {
                    float dist = (localPosition.DistanceToSquared(player.Position));
                    float maxDistance = (float)player.GetSharedData("MaxDistance");

                    if (dist < maxDistance)
                    {
                        Add(player);
                    }
                    else
                    {
                        player.VoiceVolume = 0f;
                        player.Voice3d = false;
                    }
                }
            }

            foreach (Player playerTempActive in _activePlayers.ToList())
            {
                Player player = Entities.Players.GetAt(playerTempActive.Id);

                if (player.Handle != 0)
                {
                    float dist = (localPosition.DistanceToSquared(player.Position));
                    float maxDistance = (float)player.GetSharedData("MaxDistance");

                    if (dist > maxDistance)
                        Remove(player, true);
                    else
                    {
                        player.Voice3d = true;
                        player.VoiceVolume = (1.0f - (dist / maxDistance));
                    }
                }
                else
                    Remove(player, true);
            }

            foreach (Player radioTempPlayer in _radioPlayers.ToList())
            {
                Player radioPlayer = Entities.Players.GetAt(radioTempPlayer.Id);

                if ((bool)radioPlayer.GetSharedData("IsRadioTalking"))
                {
                    radioPlayer.Voice3d = false;
                    radioPlayer.VoiceVolume = 1.0f;
                }
                else
                {
                    if (_activePlayers.ToList().All(x => x.Name != radioPlayer.Name))
                        radioPlayer.VoiceVolume = 0f;
                }
            }

            if (_phonePlayer != null)
            {
                _phonePlayer.Voice3d = false;
                _phonePlayer.VoiceVolume = 1.0f;
            }
        }

        private long UnixTimeNowInMilliseconds()
        {
            TimeSpan timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalMilliseconds;
        }

        private void Notify(string text)
        {
            Ui.SetNotificationTextEntry("STRING");
            Ui.AddTextComponentSubstringPlayerName(text);
            Ui.DrawNotification(false, false);
        }
    }
}
