﻿using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using VoiceServer.Extensions;

namespace VoiceServer.Model
{
    public class VoiceRoom
    {
        private readonly List<Player> _players;

        public float Frequence { get; private set; }

        public VoiceRoom(float frequence)
        {
            Frequence = frequence;
            _players = new List<Player>();
        }

        public void AddPlayer(Player player)
        {
            player.VoiceRoom = this;
            _players.Add(player);

            foreach (Player p in _players)
            {
                if (p.Id == player.Id)
                    continue;

                p.Client.EnableVoiceTo(player.Client);
                player.Client.EnableVoiceTo(p.Client);
                p.Client.TriggerEvent("addVoiceRadioPlayer", player.Client.Name);
                player.Client.TriggerEvent("addVoiceRadioPlayer", p.Client.Name);
            }
        }

        public void RemovePlayer(Player player)
        {
            player.VoiceRoom = null;
            _players.Remove(player);

            foreach (Player p in _players)
            {
                p.TryDisableVoiceToPlayer(player.Client);
                player.TryDisableVoiceToPlayer(p.Client);

                p.Client.TriggerEvent("removeVoiceRadioPlayer", player.Client.Name);
                player.Client.TriggerEvent("clearVoiceRadioPlayers");
            }
        }

        public bool ContainsPlayer(Client player)
        {
            return _players.Any(x => x.Client.Name == player.Name);
        }
    }
}
