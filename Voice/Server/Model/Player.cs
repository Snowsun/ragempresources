﻿using System.Collections.Generic;
using GTANetworkAPI;

namespace VoiceServer.Model
{
    public class Player
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public int TempId { get; set; }

        public Client Client { get; set; }

        public VoiceRoom VoiceRoom { get; set; }

        public HashSet<Client> RangeVoicePlayers { get; private set; }

        public Player PhonePlayer { get; set; }

        public Player()
        {
            RangeVoicePlayers = new HashSet<Client>();
        }
    }
}
