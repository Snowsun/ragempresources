﻿using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using VoiceServer.Extensions;
using VoiceServer.Model;

namespace VoiceServer
{
    public class VoiceHandler : Script
    {
        #region Properties & Fields

        private readonly List<Player> _players;
        private readonly List<VoiceRoom> _voiceRooms;

        #endregion

        #region Constructors

        public VoiceHandler()
        {
            _players = new List<Player>();
            _voiceRooms = new List<VoiceRoom>();
        }

        #endregion

        #region Events

        [ServerEvent(Event.PlayerConnected)]
        public void PlayerConnect(Client client)
        {
            _players.Add(new Player
            {
                Client = client,
                Id = _players.Count + 1,
                TempId = _players.Count + 1,
                Username = client.Name
            });

            client.SetSharedData("IsRadioTalking", false);
            client.SetSharedData("MaxDistance", 25f);
            client.TriggerEvent("playerConnected");
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void PlayerDisconnect(Client client, DisconnectionType type, string reason)
        {
            Player player = _players.FirstOrDefault(x => x.Username == client.Name);

            if (player == null)
                return;

            _players.Remove(player);
        }

        #endregion

        #region RemoteEvents

        [RemoteEvent("add_voice_listener")]
        public void AddVoiceListener(Client client, object[] args)
        {
            if (args[0] is string targetName)
            {
                Player p = _players.FirstOrDefault(x => x.Username == client.Name);
                Player t = _players.FirstOrDefault(x => x.Username == targetName);

                if (p.Id == t.Id)
                    return;

                client.EnableVoiceTo(t.Client);
                t.Client.EnableVoiceTo(client);

                p.RangeVoicePlayers.Add(t.Client);
                t.RangeVoicePlayers.Add(client);
            }
        }

        [RemoteEvent("remove_voice_listener")]
        public void RemoveVoiceListener(Client client, object[] args)
        {
            if (args[0] is string targetName)
            {
                Player p = _players.FirstOrDefault(x => x.Username == client.Name);
                Player t = _players.FirstOrDefault(x => x.Username == targetName);

                if (p.Id == t.Id)
                    return;

                if (p.RangeVoicePlayers.Contains(t.Client))
                    p.RangeVoicePlayers.Remove(t.Client);

                if (t.RangeVoicePlayers.Contains(client))
                    t.RangeVoicePlayers.Remove(client);

                p.TryDisableVoiceToPlayer(t.Client);
                t.TryDisableVoiceToPlayer(client);
            }
        }

        [RemoteEvent("SetIsRadioTalking")]
        public void SetIsRadioTalking(Client client, object[] args)
        {
            client.SetSharedData("IsRadioTalking", (bool)args[0]);
        }

        [RemoteEvent("SetMaxDistance")]
        public void SetMaxDistance(Client client, object[] args)
        {
            client.SetSharedData("MaxDistance", (float)args[0]);
        }

        #endregion

        #region Commands

        [Command("radio")]
        public void Radio(Client sender, string frequence)
        {
            Player p = _players.FirstOrDefault(x => x.Username == sender.Name);
            if (p == null)
                return;

            p.VoiceRoom?.RemovePlayer(p);

            GetVoiceRoom(float.Parse(frequence)).AddPlayer(p);
        }
        
        [Command("leaveradio")]
        public void LeaveRadio(Client sender)
        {
            Player p = _players.FirstOrDefault(x => x.Username == sender.Name);

            p?.VoiceRoom?.RemovePlayer(p);
        }

        [Command("calltest")]
        public void CallTest(Client sender, string targetName)
        {
            Player p = _players.FirstOrDefault(x => x.Username == sender.Name);
            if (p == null)
                return;

            Player target = _players.FirstOrDefault(x => x.Username == targetName);

            if (target == null)
                return;

            p.PhonePlayer = target;
            target.PhonePlayer = p;

            p.Client.EnableVoiceTo(target.Client);
            target.Client.EnableVoiceTo(p.Client);
            p.Client.TriggerEvent("changeVoicePhonePlayer", target.Client.Name);
            target.Client.TriggerEvent("changeVoicePhonePlayer", p.Client.Name);
        }

        [Command("stopcalltest")]
        public void StopCallTest(Client sender)
        {
            Player p = _players.FirstOrDefault(x => x.Username == sender.Name);

            if (p?.PhonePlayer == null)
                return;

            Player target = p.PhonePlayer;
            target.PhonePlayer = null;
            p.PhonePlayer = null;

            p.TryDisableVoiceToPlayer(target.Client);
            target.TryDisableVoiceToPlayer(p.Client);

            p.Client.TriggerEvent("changeVoicePhonePlayer", string.Empty);
            target.Client.TriggerEvent("changeVoicePhonePlayer", string.Empty);
        }

        [Command("rangetest")]
        public void RangeTest(Client sender)
        {
            Player p = _players.FirstOrDefault(x => x.Username == sender.Name);
            if (p == null)
                return;

            p.Client.TriggerEvent("voiceRangeTest");

            p.Client.SendChatMessage("Range Players Server");

            foreach (Client rangeVoicePlayer in p.RangeVoicePlayers)
            {
                p.Client.SendChatMessage($"{rangeVoicePlayer.Name}");
            }
        }

        #endregion

        #region HelperMethods

        private VoiceRoom GetVoiceRoom(float frequence)
        {
            VoiceRoom room = _voiceRooms.FirstOrDefault(x => x.Frequence == frequence);

            if (room == null)
            {
                room = new VoiceRoom(frequence);
                _voiceRooms.Add(room);
            }

            return room;
        }

        #endregion
    }
}
