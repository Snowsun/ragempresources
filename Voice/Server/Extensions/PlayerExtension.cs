﻿using GTANetworkAPI;
using VoiceServer.Model;

namespace VoiceServer.Extensions
{
    public static class PlayerExtension
    {
        public static void TryDisableVoiceToPlayer(this Player player, Client target)
        {
            if ((player.VoiceRoom == null || !player.VoiceRoom.ContainsPlayer(target)) &&
                player.PhonePlayer?.Username != target.Name &&
                !player.RangeVoicePlayers.Contains(target))
                player.Client.DisableVoiceTo(target);
        }
    }
}
