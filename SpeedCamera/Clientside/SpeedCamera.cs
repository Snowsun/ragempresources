﻿using System.Collections.Generic;
using RAGE;

namespace SpeedCameraClientside
{
    public class SpeedCamera : Events.Script
    {
        private bool _showFlash = false;
        private int _ticksToShow = 10;
        private int _tickCount = 0;

        public SpeedCamera()
        {
            Events.Add("speedCameraFlash", SpeedCameraFlash);
            Events.Tick += Tick;
        }

        private void SpeedCameraFlash(object[] args)
        {
            _tickCount = 0;
            _showFlash = true;
        }

        private void Tick(List<Events.TickNametagData> nametags)
        {
            if (_showFlash)
            {
                RAGE.Game.Graphics.DrawRect(0, 0, 2.0f, 2.0f, 255,255,255,150, 0);
                _tickCount++;
                if (_tickCount >= _ticksToShow)
                {
                    _tickCount = 0;
                    _showFlash = false;
                }
            }
        }
    }
}
