﻿using System;
using GTANetworkAPI;

namespace SpeedCameraServer
{
    public class SpeedCameraHandler : Script
    {
        [Command("createspeedcamera", Alias = "csc")]
        public void CreateSpeedCameraCommand(Client client, int speed, int range)
        {
            NAPI.Task.Run(() =>
            {
                ColShape colShape = NAPI.ColShape.CreateCylinderColShape(client.Position.Subtract(new Vector3(0, 0, 0.6)), range, 10f);
                colShape.SetData("maxspeed", speed);
                colShape.OnEntityEnterColShape += (shape, entity) =>
                {
                    if (entity.Type != EntityType.Player)
                        return;

                    if (!colShape.HasData("maxspeed"))
                        return;

                    if (!entity.IsInVehicle)
                        return;

                    Vector3 velocity = NAPI.Entity.GetEntityVelocity(entity.Vehicle);

                    int currentSpeed = (int)(Math.Sqrt(
                                                  velocity.X * velocity.X +
                                                  velocity.Y * velocity.Y +
                                                  velocity.Z * velocity.Z
                                       ) * 3.6);

                    int maxSpeed = colShape.GetData("maxspeed");

                    if (currentSpeed > maxSpeed)
                    {
                        entity.TriggerEvent("speedCameraFlash");
                        entity.SendNotification($"Too fast. Max speed {maxSpeed}");
                    }
                };

                client.SendNotification("Speed camera created");
            });
        }
    }
}
